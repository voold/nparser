<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    /**
     * Логирует запрос парсера новостей
     * @param array $params [request_method, request_url, response_http_code, response_body]
     * @return bool
    */
    public static function parserRequest(array $params): bool {
        $log = new Log();
        $log->request_method = $params['request_method'];
        $log->request_url = $params['request_url'];
        $log->response_http_code = $params['response_http_code'];
        $log->response_body = $params['response_body'];
        return $log->save();
    }
}
