<?php

namespace App\Parsers;

use Illuminate\Support\Facades\Http;
use App\Models\{Log, News};

class NewsParser
{
    /**
     * Запускает процесс получения xml, парсинга и сохранения новостей в бд
     * @param string $url – необязательный параметр, по умолчанию берется из конфига
     * @return bool
    */
    public function run(string $url = null): bool
    {
        // берем url из переданного аргумента или конфига
        $url = $url ?? config('nparser.url');

        $xml = $this->requestUrl($url);

        $newsArray = $this->convertXMLToArray($xml);
        $newsArray =  $this->parseNews($newsArray);

        return $this->saveToDB($newsArray);
    }

    /**
     * Получает страницу, логирует запрос
     * @param string $url
    */
    private function requestUrl(string $url)
    {
        $request = Http::get($url);

        Log::parserRequest([
            'request_method' => 'get',
            'request_url' => $url,
            'response_http_code' => $request->status(),
            'response_body' => $request->body()
        ]);

        return $request;
    }

    /**
     * Конвертирует XML, возвращает массив
     * @param string $xml
     */
    private function convertXMLToArray(string $xml): array
    {
        $xml = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        $xml = json_encode($xml);
        $xml = json_decode($xml, true);
        return $xml;
    }

    /**
     * Приводит новости к нужному формату для сохранения в бд
     * @param array $news
     * @return array
    */
    private function parseNews(array $news): array
    {
        $parsed = [];

        foreach($news['channel']['item'] as $item) {
            // конвертируем дату
            $date = new \Carbon\Carbon($item['pubDate']);
            $date = $date->format('Y-m-d H:i:s');

            // ищем изображение
            if($image = $item['enclosure'][0]['@attributes'] ?? false) {
                // если это изображение - добавляем
                $image = ($image['type'] == 'image/jpeg') ? $image['url'] : null;
            }

            $parsed[] = [
                'title' => $item['title'],
                'description' => $item['description'],
                'link' => $item['link'],
                'publication_date' => $date,
                'author' => $item['author'] ?? null,
                'image' => $image ?? null,
            ];

        }

        return $parsed;
    }

    /**
     * Сохраняет новости в БД
     * @param array $news — подгтовленный для инсерта массив с новостями
     * @return bool
     */
    private function saveToDB(array $news): bool
    {
        return News::insert($news);
    }
}
